package tugasakhir;

import java.util.Scanner;

public class Main {
    public static void main(String [] args){
        Integer choose;
        Scanner jam = new Scanner(System.in);

        wib utc7 = new wib();
        wita utc8 = new wita();
        wit utc9 = new wit();

        System.out.println("Pilih Zona Waktu Anda :");
        System.out.println("1.Waktu Indonesia Barat (WIB)");
        System.out.println("2.Waktu Indonesia Tengah (WITA)");
        System.out.println("3.Waktu Indonesia Timur (WIT)");
        System.out.print("Ketik 1-3 : ");
        choose = jam.nextInt();
        System.out.println(" ");
        System.out.println("Penulisan Waktu : 13.45 (Jam = 0-23, Menit = 0-59)");
        switch (choose){
            case 1 :
                System.out.println("Anda berada di bagian WIB");
                System.out.print("Masukkan waktu Anda saat ini  : ");
                utc7.jam = jam.nextDouble();
                System.out.println(" ");

                    utc7.utcmin10();
                    utc7.utcmin9();
                    utc7.utcmin8();
                    utc7.utcmin7();
                    utc7.utcmin6();
                    utc7.utcmin5();
                    utc7.utcmin4();
                    utc7.utcmin3();
                    utc7.utcmin2();
                    utc7.utcmin1();
                    utc7.utc();
                    utc7.utcplus1();
                    utc7.utcplus2();
                    utc7.utcplus3();
                    utc7.utcplus4();
                    utc7.utcplus5();
                    utc7.utcplus6();
                    utc7.utcplus7();
                    utc7.utcplus8();
                    utc7.utcplus9();
                    utc7.utcplus10();
                    utc7.utcplus11();
                    utc7.utcplus12();
                break;

            case 2 :
                System.out.println("Anda berada di bagian WITA");
                System.out.print("Masukkan waktu Anda saat ini : ");
                utc8.jam = jam.nextDouble();
                System.out.println(" ");
                    utc8.utcmin10();
                    utc8.utcmin9();
                    utc8.utcmin8();
                    utc8.utcmin7();
                    utc8.utcmin6();
                    utc8.utcmin5();
                    utc8.utcmin4();
                    utc8.utcmin3();
                    utc8.utcmin2();
                    utc8.utcmin1();
                    utc8.utc();
                    utc8.utcplus1();
                    utc8.utcplus2();
                    utc8.utcplus3();
                    utc8.utcplus4();
                    utc8.utcplus5();
                    utc8.utcplus6();
                    utc8.utcplus7();
                    utc8.utcplus8();
                    utc8.utcplus9();
                    utc8.utcplus10();
                    utc8.utcplus11();
                    utc8.utcplus12();
                break;

            case 3 :
                System.out.println("Anda berada di bagian WIT");
                System.out.print("Masukkan waktu Anda saat ini : ");
                utc9.jam = jam.nextDouble();
                System.out.println(" ");
                    utc9.utcmin10();
                    utc9.utcmin9();
                    utc9.utcmin8();
                    utc9.utcmin7();
                    utc9.utcmin6();
                    utc9.utcmin5();
                    utc9.utcmin4();
                    utc9.utcmin3();
                    utc9.utcmin2();
                    utc9.utcmin1();
                    utc9.utc();
                    utc9.utcplus1();
                    utc9.utcplus2();
                    utc9.utcplus3();
                    utc9.utcplus4();
                    utc9.utcplus5();
                    utc9.utcplus6();
                    utc9.utcplus7();
                    utc9.utcplus8();
                    utc9.utcplus9();
                    utc9.utcplus10();
                    utc9.utcplus11();
                    utc9.utcplus12();
                break;

            default:
                System.out.println("Tidak ada pilihan");
                break;
        }
    }


}
