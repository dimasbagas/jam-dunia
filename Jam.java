package tugasakhir;

public interface Jam {
    void utcmin10();
    void utcmin9();
    void utcmin8();
    void utcmin7();
    void utcmin6();
    void utcmin5();
    void utcmin4();
    void utcmin3();
    void utcmin2();
    void utcmin1();
    void utc();

}
