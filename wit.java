package tugasakhir;

import java.text.DecimalFormat;

public class wit implements Jam, Jam2 {
    DecimalFormat time = new DecimalFormat("0.00");
    double jam;
    double convert1,convert2,convert3,convert4,convert5,convert6,convert7,convert8,convert9,convert10,convert11
            ,convert12,convert13,convert14,convert15,convert16,convert17,convert18,convert19,
            convert20,convert21,convert22,convert23;

    @Override
    public void utcmin10() {
        convert1 = jam - 19;
        if (convert1 < 0) {
            convert1 += 24;
            System.out.println("UTC-10 : " + time.format(convert1));
        } else {
            System.out.println("UTC-10 : " + time.format(convert1));
        }
    }

    @Override
    public void utcmin9() {
        convert2 = jam - 18;
        if (convert2 < 0) {
            convert2 += 24;
            System.out.println("UTC-9 : " + time.format(convert2));
        } else {
            System.out.println("UTC-9 : " + time.format(convert2));
        }
    }

    @Override
    public void utcmin8() {
        convert3 = jam - 17;
        if (convert3 < 0) {
            convert3 += 24;
            System.out.println("UTC-8 : " + time.format(convert3));
        } else {
            System.out.println("UTC-8 : " + time.format(convert3));
        }
    }

    @Override
    public void utcmin7() {
        convert4 = jam - 16;
        if (convert4 < 0) {
            convert4 += 24;
            System.out.println("UTC-7 : " + time.format(convert4));
        } else {
            System.out.println("UTC-7 : " + time.format(convert4));
        }
    }

    @Override
    public void utcmin6() {
        convert5 = jam - 15;
        if (convert5 < 0) {
            convert5 += 24;
            System.out.println("UTC-6 : " + time.format(convert5));
        } else {
            System.out.println("UTC-6 : " + time.format(convert5));
        }
    }

    @Override
    public void utcmin5() {
        convert6 = jam - 14;
        if (convert6 < 0) {
            convert6 += 24;
            System.out.println("UTC-5 : " + time.format(convert6));
        } else {
            System.out.println("UTC-5 : " + time.format(convert6));
        }
    }

    @Override
    public void utcmin4() {
        convert7 = jam - 13;
        if (convert7 < 0) {
            convert7 += 24;
            System.out.println("UTC-4 : " + time.format(convert7));
        } else {
            System.out.println("UTC-4 : " + time.format(convert7));
        }
    }

    @Override
    public void utcmin3() {
        convert8 = jam - 12;
        if (convert8 < 0) {
            convert8 += 24;
            System.out.println("UTC-3 : " + time.format(convert8));
        } else {
            System.out.println("UTC-3 : " + time.format(convert8));
        }
    }

    @Override
    public void utcmin2() {
        convert9 = jam - 11;
        if (convert9 < 0) {
            convert9 += 24;
            System.out.println("UTC-2 : " + time.format(convert9));
        } else {
            System.out.println("UTC-2 : " + time.format(convert9));
        }
    }

    @Override
    public void utcmin1() {
        convert10 = jam - 10;
        if (convert10 < 0) {
            convert10 += 24;
            System.out.println("UTC-1 : " + time.format(convert10));
        } else {
            System.out.println("UTC-1 : " + time.format(convert10));
        }
    }

    @Override
    public void utc() {
        convert11 = jam - 9;
        if (convert11 < 0) {
            convert11 += 24;
            System.out.println("UTC : " + time.format(convert11));
        } else {
            System.out.println("UTC : " + time.format(convert11));
        }
    }

    @Override
    public void utcplus1() {
        convert12 = jam - 8;
        if (convert12 < 0) {
            convert12 += 24;
            System.out.println("UTC+1 : " + time.format(convert12));
        } else {
            System.out.println("UTC+1 : " + time.format(convert12));
        }
    }

    @Override
    public void utcplus2() {
        convert13 = jam - 7;
        if (convert13 < 0) {
            convert13 += 24;
            System.out.println("UTC+2 : " + time.format(convert13));
        } else {
            System.out.println("UTC+2 : " + time.format(convert13));
        }
    }

    @Override
    public void utcplus3() {
        convert14 = jam - 6;
        if (convert14 < 0) {
            convert14 += 24;
            System.out.println("UTC+3 : " + time.format(convert14));
        }else{
            System.out.println("UTC+3 : " + time.format(convert14));
        }
    }

    @Override
    public void utcplus4() {
        convert15 = jam - 5;
        if (convert15 < 0) {
            convert15 += 24;
            System.out.println("UTC+4 : " + time.format(convert15));
        } else {
            System.out.println("UTC+4 : " + time.format(convert15));
        }
    }

    @Override
    public void utcplus5() {
        convert16 = jam - 4;
        if (convert16 < 0) {
            convert16 += 24;
            System.out.println("UTC+5 : " + time.format(convert16));
        } else {
            System.out.println("UTC+5 : " + time.format(convert16));
        }
    }

    @Override
    public void utcplus6() {
        convert17 = jam - 3;
        if (convert17 < 0) {
            convert17 += 24;
            System.out.println("UTC+6 : " + time.format(convert17));
        } else {
            System.out.println("UTC+6 : " + time.format(convert17));
        }
    }

    @Override
    public void utcplus7() {
        convert18 = jam - 2;
        if (convert18 < 0) {
            convert18 += 24;
            System.out.println("UTC+7 : " + time.format(convert18));
        } else {
            System.out.println("UTC+7 : " + time.format(convert18));
        }
    }

    @Override
    public void utcplus8() {
        convert19 = jam - 1;
        if (convert19 < 0) {
            convert19 += 24;
            System.out.println("UTC+8 : " + time.format(convert19));
        } else {
            System.out.println("UTC+8 : " + time.format(convert19));
        }
    }

    @Override
    public void utcplus9() {
        convert20 = jam + 0;
        System.out.println("UTC+9 : " + time.format(convert20));
    }

    @Override
    public void utcplus10() {
        convert21 = jam + 1;
        if (convert21 > 24) {
            convert21 -= 24;
            System.out.println("UTC+10 : " + time.format(convert21));
        } else {
            System.out.println("UTC+10 : " + time.format(convert21));
        }
    }

    @Override
    public void utcplus11() {
        convert22 = jam + 2;
        if (convert22 > 24) {
            convert22 -= 24;
            System.out.println("UTC+11 : " + time.format(convert22));
        } else {
            System.out.println("UTC+11 : " + time.format(convert22));
        }
    }

    @Override
    public void utcplus12() {
        convert23 = jam + 3;
        if (convert23 > 24) {
            convert23 -= 24;
            System.out.println("UTC+12 : " + time.format(convert23));
        } else {
            System.out.println("UTC+12 : " + time.format(convert23));
        }

}
}