package tugasakhir;

import javax.swing.*;
import java.text.DecimalFormat;

public class jamwita {
    private JPanel JamWITAPanel;
    private JTextField tfJam;
    private JTextField tfUTCPlus5;
    private JTextField tfUTCPlus6;
    private JTextField tfUTCPlus8;
    private JTextField tfUTCPlus9;
    private JButton konversiButton;
    private JTextField tfUTCPlus10;
    private JTextField tfUTCPlus11;
    private JTextField tfUTCPlus12;
    private JTextField tfUTCPlus7;
    private JTextField tfUTCPlus4;
    private JTextField tfUTCPlus3;
    private JTextField tfUTCPlus2;
    private JTextField tfUTCPlus1;
    private JTextField tfUTC;
    private JTextField tfUTCMinus1;
    private JTextField tfUTCMinus2;
    private JTextField tfUTCMinus3;
    private JTextField tfUTCMinus4;
    private JTextField tfUTCMinus5;
    private JTextField tfUTCMinus6;
    private JTextField tfUTCMinus8;
    private JTextField tfUTCMinus7;
    private JTextField tfUTCMinus10;
    private JTextField tfUTCMinus9;

    public jamwita() {
        konversiButton.addActionListener(e -> {
            double jam,utcminsepuluh,utcminsembilan,utcmindelapan,utcmintujuh,utcminenam,
                    utcminlima,utcminempat,utcmintiga,utcmindua,utcminsatu,utc,utcplussatu,utcplusdua,
                    utcplustiga,utcplusempat,utcpluslima,utcplusenam,utcplustujuh,utcplusdelapan,utcplussemblian
                    ,utcplussepuluh,utcplussebelas,utcplusduabelas;
            DecimalFormat time = new DecimalFormat("0.00");

            jam = Double.parseDouble(tfJam.getText());

            utcminsepuluh = jam - 18;
            if (utcminsepuluh <0){
                utcminsepuluh +=24;
                time.format(utcminsepuluh);
            }else {
                time.format(utcminsepuluh);
            }

            utcminsembilan = jam - 17;
            if (utcminsembilan <0){
                utcminsembilan +=24;
                time.format(utcminsembilan);
            }else {
                time.format(utcminsembilan);
            }

            utcmindelapan = jam - 16;
            if (utcmindelapan <0){
                utcmindelapan +=24;
                time.format(utcmindelapan);
            }else {
                time.format(utcmindelapan);
            }

            utcmintujuh = jam - 15;
            if (utcmintujuh <0){
                utcmintujuh +=24;
                time.format(utcmintujuh);
            }else {
                time.format(utcmintujuh);
            }

            utcminenam = jam - 14;
            if (utcminenam <0){
                utcminenam +=24;
                time.format(utcminenam);
            }else {
                time.format(utcminenam);
            }

            utcminlima = jam - 13;
            if (utcminlima <0){
                utcminlima +=24;
                time.format(utcminlima);
            }else {
                time.format(utcminlima);
            }

            utcminempat = jam - 12;
            if (utcminempat <0){
                utcminempat +=24;
                time.format(utcminempat);
            }else {
                time.format(utcminempat);
            }

            utcmintiga = jam - 11;
            if (utcmintiga <0){
                utcmintiga +=24;
                time.format(utcmintiga);
            }else {
                time.format(utcmintiga);
            }

            utcmindua = jam - 10;
            if (utcmindua <0){
                utcmindua +=24;
                time.format(utcmindua);
            }else {
                time.format(utcmindua);
            }

            utcminsatu = jam - 9;
            if (utcminsatu <0){
                utcminsatu +=24;
                time.format(utcminsatu);
            }else {
                time.format(utcminsatu);
            }

            utc = jam - 8;
            if (utc <0){
                utc +=24;
                time.format(utc);
            }else {
                time.format(utc);
            }

            utcplussatu = jam - 7;
            if (utcplussatu <0){
                utcplussatu +=24;
                time.format(utcplussatu);
            }else {
                time.format(utcplussatu);
            }

            utcplusdua = jam - 6;
            if (utcplusdua <0){
                utcplusdua +=24;
                time.format(utcplusdua);
            }else {
                time.format(utcplusdua);
            }

            utcplustiga = jam - 5;
            if (utcplustiga <0){
                utcplustiga +=24;
                time.format(utcplustiga);
            }else {
                time.format(utcplustiga);
            }

            utcplusempat = jam - 4;
            if (utcplusempat <0){
                utcplusempat +=24;
                time.format(utcplusempat);
            }else {
                time.format(utcplusempat);
            }

            utcpluslima = jam - 3;
            if (utcpluslima <0){
                utcpluslima +=24;
                time.format(utcpluslima);
            }else {
                time.format(utcpluslima);
            }

            utcplusenam = jam - 2;
            if (utcplusenam <0){
                utcplusenam +=24;
                time.format(utcplusenam);
            }else {
                time.format(utcplusenam);
            }

            utcplustujuh = jam - 1;
            if (utcplustujuh <0){
                utcplustujuh +=24;
                time.format(utcplustujuh);
            }else {
                time.format(utcplustujuh);
            }

            utcplusdelapan = jam - 0;
                    time.format(utcplusdelapan);

            utcplussemblian = jam + 1;
            if (utcplussemblian > 24){
                utcplussemblian -=24;
                time.format(utcplussemblian);
            }else {
                time.format(utcplussemblian);
            }

            utcplussepuluh = jam + 2;
            if (utcplussepuluh > 24){
                utcplussepuluh -=24;
                time.format(utcplussepuluh);
            }else {
                time.format(utcplussepuluh);
            }

            utcplussebelas = jam + 3;
            if (utcplussebelas > 24){
                utcplussebelas -=24;
                time.format(utcplussebelas);
            }else {
                time.format(utcplussebelas);
            }

            utcplusduabelas = jam + 4;
            if (utcplusduabelas > 24){
                utcplusduabelas -=24;
                time.format(utcplusduabelas);
            }else {
                time.format(utcplusduabelas);
            }

            tfUTCMinus10.setText(String.valueOf(utcminsepuluh));
            tfUTCMinus9.setText(String.valueOf(utcminsembilan));
            tfUTCMinus8.setText(String.valueOf(utcmindelapan));
            tfUTCMinus7.setText(String.valueOf(utcmintujuh));
            tfUTCMinus6.setText(String.valueOf(utcminenam));
            tfUTCMinus5.setText(String.valueOf(utcminlima));
            tfUTCMinus4.setText(String.valueOf(utcminempat));
            tfUTCMinus3.setText(String.valueOf(utcmintiga));
            tfUTCMinus2.setText(String.valueOf(utcmindua));
            tfUTCMinus1.setText(String.valueOf(utcminsatu));
            tfUTC.setText(String.valueOf(utc));
            tfUTCPlus1.setText(String.valueOf(utcplussatu));
            tfUTCPlus2.setText(String.valueOf(utcplusdua));
            tfUTCPlus3.setText(String.valueOf(utcplustiga));
            tfUTCPlus4.setText(String.valueOf(utcplusempat));
            tfUTCPlus5.setText(String.valueOf(utcpluslima));
            tfUTCPlus6.setText(String.valueOf(utcplusenam));
            tfUTCPlus7.setText(String.valueOf(utcplustujuh));
            tfUTCPlus8.setText(String.valueOf(utcplusdelapan));
            tfUTCPlus9.setText(String.valueOf(utcplussemblian));
            tfUTCPlus10.setText(String.valueOf(utcplussepuluh));
            tfUTCPlus11.setText(String.valueOf(utcplussebelas));
            tfUTCPlus12.setText(String.valueOf(utcplusduabelas));
        }
        );
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("WITA");
        frame.setContentPane(new jamwita().JamWITAPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}
